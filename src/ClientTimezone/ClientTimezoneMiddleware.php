<?php

namespace Gsdk\Middleware\ClientTimezone;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class ClientTimezoneMiddleware
{
    protected string $defaultTimezone = 'UTC';

    protected string $cookieName = 'timezone';

    public function handle(Request $request, Closure $next)
    {
        try {
            date_default_timezone_set($this->getClientTimezone());
        } catch (\ErrorException $e) {
            date_default_timezone_set($this->defaultTimezone);
        }

        $request->attributes->add(['timezone' => date_default_timezone_get()]);

        $this->bootTimezone();

        return $next($request);
    }

    protected function bootTimezone(): void
    {
        DB::update('SET time_zone = ?', [date('P')]);
    }

    private function getClientTimezone(): string
    {
        return Cookie::get($this->cookieName) ?? $this->defaultTimezone;
    }
}