<?php

namespace Gsdk\Middleware\LastModified;

use Closure;
use DateTimeInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Carbon;

class LastModifiedMiddleware
{
    protected array $except = [];

    public function handle(Request $request, Closure $next): Response
    {
        if (
            !$this->isSupportedMethod($request)
            || $this->inUrlExcluded($request)
            || $this->isDisabled($request)
        ) {
            return $next($request);
        }

        return $this->processAfterController($request, $next($request));
    }

    protected function processAfterController(Request $request, Response $response): Response
    {
        if (!LastModifiedDate::isset()) {
            return $response;
        }

        $updatedAt = LastModifiedDate::get();

        $this->appendLastModifiedHeader($response, $updatedAt);

        $this->abortIfModifiedSince($request, $updatedAt);

        return $response;
    }

    protected function isSupportedMethod(Request $request): bool
    {
        return in_array(strtoupper($request->getMethod()), ['GET', 'HEAD']);
    }

    protected function inUrlExcluded(Request $request): bool
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

    protected function isDisabled(Request $request): bool
    {
        return false;
    }

    protected function appendLastModifiedHeader(Response $response, DateTimeInterface $updatedAt): void
    {
        $response->headers->set('Last-Modified', $updatedAt->format(DATE_RFC7231));
    }

    protected function abortIfModifiedSince(Request $request, DateTimeInterface $updatedAt): void
    {
        if (!$request->hasHeader('If-Modified-Since')) {
            return;
        }

        $modifiedSince = Carbon::createFromTimeString($request->header('If-Modified-Since'), 'GMT');
        if ($updatedAt->getTimestamp() <= $modifiedSince->getTimestamp()) {
            abort(304);
        }
    }
}
