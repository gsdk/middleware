<?php

namespace Gsdk\Middleware\LastModified;

use DateTime;
use DateTimeInterface;

abstract class LastModifiedDate
{
    private static ?DateTimeInterface $updatedAt;

    public static function isset(): bool
    {
        return isset(static::$updatedAt);
    }

    public static function set(string|int|DateTimeInterface $updatedAt): void
    {
        if (is_string($updatedAt)) {
            $updatedAt = new DateTime($updatedAt);
        } elseif (is_int($updatedAt)) {
            $timestamp = $updatedAt;
            $updatedAt = new DateTime();
            $updatedAt->setTimestamp($timestamp);
        }

        static::$updatedAt = $updatedAt;
    }

    public static function get(): ?DateTimeInterface
    {
        return static::$updatedAt;
    }
}
