<?php

namespace Gsdk\Middleware\SecurityHeaders;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SecurityHeadersMiddleware
{
    protected const X_FRAME_OPTIONS_DENY        = 'DENY';
    protected const X_FRAME_OPTIONS_SAME_ORIGIN = 'SAMEORIGIN';

    protected const REFERRER_POLICY_NO_REFERRER                     = 'no-referrer';
    protected const REFERRER_POLICY_NO_REFERRER_WHEN_DOWNGRADE      = 'no-referrer-when-downgrade';
    protected const REFERRER_POLICY_ORIGIN                          = 'origin';
    protected const REFERRER_POLICY_ORIGIN_WHEN_CROSS_ORIGIN        = 'origin-when-cross-origin';
    protected const REFERRER_POLICY_SAME_ORIGIN                     = 'same-origin';
    protected const REFERRER_POLICY_STRICT_ORIGIN                   = 'strict-origin';
    protected const REFERRER_POLICY_STRICT_ORIGIN_WHEN_CROSS_ORIGIN = 'strict-origin-when-cross-origin';
    protected const REFERRER_POLICY_UNSAFE_URL                      = 'unsafe-url';

    protected array $ContentSecurityPolicy = [];

    protected array $PermissionsPolicy = [];

    protected array $headers = [];

    public function handle(Request $request, Closure $next)
    {
        if (!$this->isEnabled()) {
            return $next($request);
        }

        $this->configure($request);

        return $next($request)
            ->withHeaders($this->headers);
    }

    protected function isEnabled(): bool
    {
        return App::environment('production');
    }

    protected function configure(Request $request): void
    {
        if ($request->isSecure()) {
            $this->enableStrictTransportSecurity();
        }

        $this->enableXContentTypeOptions();

        $this->useReferrerPolicy();

        $this->useXFrameOptions();

        if ($this->ContentSecurityPolicy) {
            $this->useContentSecurityPolicy($this->ContentSecurityPolicy);
        }

        if ($this->PermissionsPolicy) {
            $this->usePermissionsPolicy($this->PermissionsPolicy);
        }
    }

    /**
     * Настраивает уровень детализации для включения в заголовок Referer при уходе со страницы. Помогает
     * предотвратить утечку данных на сайты, куда идут ссылки. Настоятельно рекомендуется.
     *
     * @param string $policy
     * @return void
     */
    protected function useReferrerPolicy(string $policy = self::REFERRER_POLICY_ORIGIN_WHEN_CROSS_ORIGIN): void
    {
        $this->headers['Referrer-Policy'] = $policy;
    }

    /**
     * Content Security Policy (CSP, политика защиты контента) — это механизм обеспечения безопасности,
     * с помощью которого можно защищаться от атак с внедрением контента, например, межсайтового скриптинга
     * (XSS, cross site scripting)
     *
     * @param array $directives
     * @return void
     */
    protected function useContentSecurityPolicy(array $directives): void
    {
        $header = '';
        foreach ($directives as $k => $v) {
            $header .= "$k $v; ";
        }
        $this->headers['Content-Security-Policy'] = trim($header);
    }

    /**
     * Permissions Policy похожа на Content Security Policy, но контролирует функции браузера вместо безопасности
     * контента. Permissions Policy содержит директивы, в которых указываются ключи и списки источников,
     * ограничивающие использование возможностей этих директив.
     *
     * @param array $policies
     * @return void
     */
    protected function usePermissionsPolicy(array $policies): void
    {
        $a = [];
        foreach ($policies as $k => $v) {
            $a[] = "$k=($v)";
        }
        $this->headers['Permissions-Policy'] = implode(', ', $a);
    }

    /**
     * HTTP Strict Transport Security (HSTS) - Данная политика безопасности позволяет сразу же устанавливать
     * безопасное соединение, вместо использования HTTP
     *
     * @param int $maxAge
     * @param bool $includeSubdomains
     * @param bool $preload
     * @return void
     */
    protected function enableStrictTransportSecurity(
        int $maxAge = 31536000,
        bool $includeSubdomains = true,
        bool $preload = true
    ): void {
        $this->headers['Strict-Transport-Security'] = 'max-age=' . $maxAge
            . ($includeSubdomains ? '; includeSubdomains' : '')
            . ($preload ? '; preload' : '');
    }

    /**
     * HTTP-заголовок ответа X-Content-Type-Options является маркером, используемым сервером для указания того,
     * что типы MIME, объявленные в заголовках Content-Type, должны соблюдаться и не изменяться.
     *
     * @return void
     */
    protected function enableXContentTypeOptions(): void
    {
        $this->headers['X-Content-Type-Options'] = 'nosniff';
    }

    /**
     * Заголовок ответа HTTP X-Frame-Options можно использовать, чтобы указать, следует ли разрешить браузеру
     * отображать страницу в <frame>
     *
     * @param string $options
     * @return void
     */
    protected function useXFrameOptions(string $options = self::X_FRAME_OPTIONS_SAME_ORIGIN): void
    {
        $this->headers['X-Frame-Options'] = $options;
    }
}
