<?php

namespace Gsdk\Middleware\CrawlerDetector;

use Closure;
use Illuminate\Http\Request;

class CrawlerDetectorMiddleware
{
    private const BOTS = [
        'bot',
        'slurp',
        'crawler',
        'spider',
        'curl',
        'facebook',
        'fetch',
        'okhttp',
        'Chrome-Lighthouse'
    ];

    public function handle(Request $request, Closure $next)
    {
        $userAgent = $request->header('User-Agent');

        if (is_string($userAgent) && $this->isUserAgentBelongsToCrawler($userAgent)) {
            $this->crawlerDetected($request);
        }

        return $next($request);
    }

    protected function crawlerDetected(Request $request): void
    {
        $request->attributes->add(['isCrawlerDetected' => true]);
    }

    protected function isUserAgentBelongsToCrawler(string $userAgent): bool
    {
        foreach (self::BOTS as $bot) {
            if (stripos($userAgent, $bot) !== false) {
                return true;
            }
        }

        return false;
    }
}
